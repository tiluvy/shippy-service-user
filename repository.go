package main

import (
	pb "bitbucket.org/tiluvy/shippy-service-user/proto/user"
	"github.com/jinzhu/gorm"
	"log"
)

type UserRepository struct {
	db *gorm.DB
}

func (repo *UserRepository) GetAll() ([]*pb.User, error) {
	var users []*pb.User
	if err := repo.db.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (repo *UserRepository) Get(id string) (*pb.User, error) {
	var user *pb.User
	user.Id = id
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *UserRepository) GetByEmailAndPassword(user *pb.User) (*pb.User, error) {
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *UserRepository) Create(user *pb.User) error {
	log.Print(user)
	if err := repo.db.Create(user).Error; err != nil {
		return err
	}
	return nil
}
//
//
//type UserRepository struct {
//	table gorm.DB
//}
//
//func (repo *UserRepository) Create (user *pb.User) error {
//	repo.table.Create(user)
//	return nil
//}
//
//func (repo *UserRepository) Update (user *pb.User) error {
//	var checkUser pb.User
//	repo.table.First(&checkUser, user.Id)
//	repo.table.UpdateColumns(user)
//	return nil
//}
//
//func (repo *UserRepository) Get (id uint) (pb.User,error) {
//	var checkUser pb.User
//	repo.table.First(&checkUser, id)
//	return checkUser, nil
//}
//
//func (repo *UserRepository) GetAll () ([]pb.User, error) {
//	var users []pb.User
//	repo.table.Find(&users)
//	return users, nil
//
//}
//func (repo *UserRepository) Auth (user *pb.User) ( pb.Token, error) {
//
//
//}
//
//func (repo *UserRepository) ValidateToken (token *pb.Token) ( pb.Token, error) {
//
//}
//


