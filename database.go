package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"fmt"
)

func CreateConnection() (*gorm.DB, error) {
	//host := os.Getenv("DB_HOST")
	//user := os.Getenv("DB_USER")
	//DBName := os.Getenv("DB_NAME")
	//password := os.Getenv("DB_PASSWORD")
	//port := "3306"

	host := "localhost"
	user := "root"
	DBName := "shippy"
	password := "adqsda"
	port := "3306"

	uri := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", user, password, host, port, DBName)
	db, err := gorm.Open("mysql", uri)
	return db, err
}