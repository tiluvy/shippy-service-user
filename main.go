package main

import (
	"fmt"
	"log"
	pb "bitbucket.org/tiluvy/shippy-service-user/proto/user"
	"github.com/micro/go-micro"
	"time"
)

type User struct {
	Id uint				`gorm:"primary_key"`
	Name string			`gorm:"type:varchar(100)" json:"name"`
	Company string		`gorm:"type:varchar(100)" json:"company"`
	Email string		`gorm:"type:varchar(100)" json:"email"`
	Password string		`gorm:"type:varchar(100)" json:"password"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
	XxxUnrecognized string    `gorm:"type:varchar(100)" json:"xxx_unrecognized"`
	XxxSizecache string    `gorm:"type:varchar(100)" json:"xxx_sizecache"`
}

func main() {

	// Creates a database connection and handles
	// closing it again before exit.
	db, err := CreateConnection()
	defer db.Close()

	if err != nil {
		log.Fatalf("Could not connect to DB: %v", err)
	}

	// Automatically migrates the user struct
	// into database columns/types etc. This will
	// check for changes and migrate them each time
	// this service is restarted.
	db.AutoMigrate(&User{})

	repo := &UserRepository{db}

	tokenService := &TokenService{repo}

	// Create a new service. Optionally include some options here.
	srv := micro.NewService(

		// This name must match the package name given in your protobuf definition
		micro.Name("shippy.user.vessel"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Register handler
	pb.RegisterUserServiceHandler(srv.Server(), &service{repo, tokenService})

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}