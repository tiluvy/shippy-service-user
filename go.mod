module bitbucket.org/tiluvy/shippy-service-user

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/jinzhu/gorm v1.9.11 // indirect
	github.com/micro/go-micro v1.11.3
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/net v0.0.0-20191011234655-491137f69257
)
